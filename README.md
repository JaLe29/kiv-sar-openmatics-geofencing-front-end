# KIV/SAR - OpenMatics - Geofencing FRONT END
## Instal NodeJS
8.2.1 and higher

Check your current version
```bash
node -v
```
## Install Dependencies

Run in the project root folder:

```bash
npm install
npm install -g @angular/cli@latest
```
## Run development server

Run in the project root folder:

```bash
ng serve --open
```

Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run :

```bash
ng build
```

To build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Developers
* Duc Vuong Tran, <<tran.tony7@gmail.com>>
* Jakub Löffelmann, <<jakubloffelmann@gmail.com>>
* Jakub Hain, <<jakub.hain@gmail.com>>