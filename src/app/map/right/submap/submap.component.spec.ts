import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmapComponent } from './submap.component';

describe('SubmapComponent', () => {
	let component: SubmapComponent;
	let fixture: ComponentFixture<SubmapComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [SubmapComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SubmapComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
