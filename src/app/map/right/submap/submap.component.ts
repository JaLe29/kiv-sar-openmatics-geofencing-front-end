import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { } from '@types/googlemaps';
import { Shape } from '../../../shared/classes/shape';
import { CreateareaService } from '../../../shared/services/createarea.service';
import { DisplayareaService } from '../../../shared/services/displayarea.service';
declare var google;
@Component({
	selector: 'app-submap',
	templateUrl: './submap.component.html',
	styleUrls: ['./submap.component.css']
})
export class SubmapComponent implements OnInit, AfterViewInit, OnDestroy {
	mapa: google.maps.Map;
	drawingManager: google.maps.drawing.DrawingManager;
	area: Shape;
	activate: Boolean;
	currentArea: any;
	drawnAreas: any[] = [];
	constructor(private createArea: CreateareaService, private displayArea: DisplayareaService) {
		this.createArea.currentShape.subscribe(shape => { this.area = shape; });
		this.createArea.activateDrawing.subscribe(activate => {
			this.activate = activate;
			if (this.activate === true) {
				this.startDrawing();
			}
			if (this.activate === false) {
				this.endDrawing();
			}
		}
		);
		this.displayArea.currentArea.subscribe(areaa => {
			this.reset();
			this.currentArea = areaa;
			if (areaa && areaa !== '') {
				this.drawShape();
			}
		});
	}
	ngOnDestroy() {
		this.displayArea.changeArea('');
	}
	reset() {
		for (let i = 0; i < this.drawnAreas.length; i++) {
			this.drawnAreas[i].setMap(null);
		}
	}

	ngAfterViewInit() {
		if (this.mapa == null) {
			this.mapa = new google.maps.Map(document.getElementById('mapa'), {
				center: { lat: 30, lng: 25 },
				zoom: 2
			});
			this.initialize();
			const self = this;
			google.maps.event.addListener(this.drawingManager, 'overlaycomplete', function (event) {
				if (self.area.shape != null) {
					self.area.shape.setMap(null);
					self.area.shape = null;
				}
				if (event.type === 'circle') {
					self.area.type = 'CIRCLE';
				}
				if (event.type === 'rectangle') {
					self.area.type = 'RECTANGLE';
				}
				if (event.type === 'polygon') {
					self.area.type = 'POLYGON';
				}
				self.drawingManager.setDrawingMode(null);
				self.area.shape = event.overlay;
			});
		}
	}
	ngOnInit() {

	}
	initialize() {
		this.drawingManager = new google.maps.drawing.DrawingManager({
			drawingMode: null,
			drawingControl: true,
			drawingControlOptions: {
				position: google.maps.ControlPosition.TOP_CENTER,
				drawingModes: ['circle', 'polygon', 'rectangle'],
			},
			rectangleOptions: {
				fillColor: '#ffff00',
				fillOpacity: 0.25,
				strokeWeight: 1,
				clickable: true,
				editable: true,
				zIndex: 1
			},
			polygonOptions: {
				fillColor: '#ffff00',
				fillOpacity: 0.25,
				strokeWeight: 1,
				clickable: true,
				editable: true,
				zIndex: 1
			},
			circleOptions: {
				fillColor: '#ffff00',
				fillOpacity: 0.25,
				strokeWeight: 1,
				clickable: true,
				editable: true,
				zIndex: 1
			}
		});
		// this.drawingManager.setMap(this.mapa);

	}
	enableDrawingManager() {
		if (this.drawingManager != null) {
			this.drawingManager.setMap(this.mapa);
		}
	}
	disableDrawingManager() {
		if (this.drawingManager != null) {
			this.drawingManager.setMap(null);
		}
	}

	startDrawing() {

		this.enableDrawingManager();
		// remove all overlays
	}

	endDrawing() {
		// check if area created
		if (this.area.shape == null) {
		} else {
			this.area.shape.setMap(null);
		}
		this.disableDrawingManager();
		// return created area
	}

	drawShape() {
		const name = this.currentArea.type;
		if (name === 'RECTANGLE') {
			this.drawRectangle(this.currentArea.positionList);
		} else if (name === 'POLYGON') {
			this.drawPolygon(this.currentArea.positionList);
		} else if (name === 'CIRCLE') {
			this.drawCircle(this.currentArea.positionList);
		}

		// this.map.setCenter(new google.maps.LatLng(topLeftLat, topLeftLong));
		this.mapa.setZoom(2);
		/* this.currentArea = new google.maps.Polygon({
			 paths: [{lat: -25.363, lng: 131.044}, {lat: -25.363, lng: 121.044}, {lat: -22.363, lng: 121.044}],
			 map: this.map,
			 fillColor: '#FF0000'
		 });*/
	}

	drawRectangle(positions: any[]) {
		const botLeftLat: number = positions[0].latitude;
		const botLeftLong: number = positions[0].longitude;
		const topRightLat: number = positions[1].latitude;
		const topRightLong: number = positions[1].longitude;
		const rect = new google.maps.Rectangle({
			strokeColor: '#FF0000',
			strokeOpacity: 0.8,
			strokeWeight: 2,
			fillColor: '#FF0000',
			fillOpacity: 0.35,
			map: this.mapa,
			bounds: {
				north: topRightLat,
				south: botLeftLat,
				east: topRightLong,
				west: botLeftLong
			}
		});
		this.drawnAreas.push(rect);
	}
	drawPolygon(positions: any[]) {
		const paths: google.maps.LatLng[] = [];
		for (let i = 0; i < positions.length; i++) {
			paths.push(new google.maps.LatLng(positions[i].latitude, positions[i].longitude));
		}
		const polygon = new google.maps.Polygon({
			strokeColor: '#FF0000',
			strokeOpacity: 0.8,
			strokeWeight: 2,
			fillColor: '#FF0000',
			fillOpacity: 0.35,
			map: this.mapa,
			paths: paths
		});
		this.drawnAreas.push(polygon);
	}
	drawCircle(positions: any[]) {
		const paths: google.maps.LatLng[] = [];
		const centerLat: number = positions[0].latitude;
		const centerLong: number = positions[0].longitude;
		const radius: any =
			Math.sqrt(Math.pow((centerLat - positions[1].latitude), 2) + Math.pow((centerLong - positions[1].longitude), 2));
		const center: google.maps.LatLng = new google.maps.LatLng(centerLat, centerLong);
		const dist =
			google.maps.geometry.spherical.computeDistanceBetween
				(center, new google.maps.LatLng(positions[1].latitude, positions[1].longitude));

		const circle = new google.maps.Circle({
			strokeColor: '#FF0000',
			strokeOpacity: 0.8,
			strokeWeight: 2,
			fillColor: '#FF0000',
			fillOpacity: 0.35,
			map: this.mapa,
			center: center,
			radius: dist
		});
		this.drawnAreas.push(circle);
	}


}


