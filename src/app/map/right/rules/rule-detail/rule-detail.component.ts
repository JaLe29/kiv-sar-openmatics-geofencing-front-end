import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Rule } from '../../../../shared/classes/rule';

@Component({
	selector: 'app-rule-detail',
	templateUrl: './rule-detail.component.html',
	styleUrls: ['./rule-detail.component.css']
})
export class RuleDetailComponent implements OnInit {
	rule: Rule;
	vehicleNames: string[] = [];
	fleetNames: string[] = [];
	constructor(
		public dialogRef: MatDialogRef<RuleDetailComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any) {
		this.rule = data.rule;

		this.rule.vehicleList.forEach(e1 => {
			let name: string = "Unknown";
			for (var i = 0; i < this.data.vehicleList.length; i++) {
				var e2 = this.data.vehicleList[i];
				if (e1 == e2.id) {
					name = e2.name;
					break;
				};
			}
			this.vehicleNames.push(name);
		});

		this.rule.fleetList.forEach(e1 => {
			let name: string = "Unknown";
			for (var i = 0; i < this.data.fleetList.length; i++) {
				var e2 = this.data.fleetList[i];
				if (e1 == e2.id) {
					name = e2.name;
					break;
				};
			}
			this.fleetNames.push(name);
		});
	}


	ngOnInit() {
	}

	getValidDate(): string {
		let result: string = "From ";
		if (this.rule.dateFrom != undefined && this.rule.dateFrom != null) {
			result += (this.rule.dateFrom.getMonth() + 1) + "/" + this.rule.dateFrom.getDate() + "/" + this.rule.dateFrom.getFullYear();
		} else {
			result += "No specific date";
		}
		result += " to ";
		if (this.rule.dateTo != undefined && this.rule.dateTo != null) {
			result += (this.rule.dateTo.getMonth() + 1) + "/" + this.rule.dateTo.getDate() + "/" + this.rule.dateTo.getFullYear();
		} else {
			result += "infinity";
		}
		return result;
	}

	getEvents(): string {
		let result: string;
		if (this.rule.type == 'inside') {
			result = 'Inside';
		} else {
			result = 'Outside';
		}
		return result;
	}

	getTimeTable(): string[] {
		let result = [];

		if (this.rule.weekValue === 2) {
			let workdays = this.rule.workdays;
			let weekends = this.rule.weekends;
			workdays.forEach(element => {
				if (element.checked) {
					result.push(element);
				}
			});

			weekends.forEach(element => {
				if (element.checked) {
					result.push(element);
				}
			});
		}

		return result;
	}

	getWholeWeekTime(): string {
		let result: string;
		let wholeWeek = this.rule.wholeWeek;
		result = ["From", wholeWeek.startTime, "to", wholeWeek.endTime].join(" ");
		return result;
	}

	getDesc(): string {
		return this.rule.description;
	}

	onCloseClick() {
		this.dialogRef.close();
	}

}
