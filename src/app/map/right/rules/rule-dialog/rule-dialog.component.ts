import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { Rule } from '../../../../shared/classes/rule';
import { Vehicle, Fleet } from '../../../../shared/classes/fleet'
import { environment } from './../../../../../environments/environment'


const timeFormat12: string = "^(0[0-9]|1[0-1]|[0-9]):([0-5][0-9])(:[0-5][0-9])?$";
const timeFormat24: string = "^(2[0-3]|[0-1]?[0-9]):([0-5][0-9])(:[0-5][0-9])?$";
const dateFormat: string = "^(0?[1-9]|1[0-2])([/])(0?[1-9]|1[0-9]|2[0-9]|3[0,1])([/])(19|20|21)[0-9]{2}$";
const PREFIX_DUPLICATE_NAME: string = "Duplicate - ";


@Component({
	selector: 'app-rule-dialog',
	templateUrl: './rule-dialog.component.html',
	styleUrls: ['./rule-dialog.component.css']
})
export class RuleDialogComponent implements OnInit {
	timeFormat: string = timeFormat24; // actual time format
	dateRegex: RegExp;

	errorMessage: string;
	dialogMode: string;
	rule: Rule; // the Rule object

	// fleets test
	fleets = new FormControl();
	vehicles = new FormControl();
	fleetList: Fleet[];
	vehicleList: Vehicle[];

	constructor(
		public dialogRef: MatDialogRef<RuleDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any) {
		// check mode - new rule/edit rule/duplicate rule
		this.dialogMode = data.mode;
		this.rule = data.data;
		this.fleetList = data.fleetList;
		this.vehicleList = data.vehicleList;
		this.dateRegex = new RegExp(dateFormat);
		if (this.isEditDialog()) {
			data.result.id = this.rule.id;
		} else {
			if (this.isDuplicateDialog()) {
				this.rule.name = PREFIX_DUPLICATE_NAME + this.rule.name;
			}
		}
		this.rule.areaId = data.areaId;
	}


	onOkClick(): void {
		//alert(this.rule.vehicleList.length+" - "+this.rule.fleetList.length);
		if (this.checkErrors()) {
			this.data.result.active = true;
			this.data.result.id = this.rule.id;
			this.data.result.name = this.rule.name;
			this.data.result.description = (this.rule.description) ? this.rule.description : "";
			this.data.result.validity.validationInterval.dateFrom = this.daysFromEpoch(this.rule.dateFrom, true);
			this.data.result.validity.validationInterval.dateTo = this.daysFromEpoch(this.rule.dateTo, false);

			this.data.result.type = this.rule.type.toUpperCase();
			// add workdays and weekends
			if (this.rule.weekValue > 1) {
				for (let i = 0; i < this.rule.workdays.length; i++) {
					let day: any = this.rule.workdays[i];
					if (day.checked) {
						this.data.result.validity.timeIntervals[i].push({
							timeFrom: this.timeToTimestamp(day.startTime),
							timeTo: this.timeToTimestamp(day.endTime)
						});
					}

				}
				for (let i = 0; i < this.rule.weekends.length; i++) {
					let day: any = this.rule.weekends[i];
					if (day.checked) {
						this.data.result.validity.timeIntervals[i + this.rule.workdays.length].push({
							timeFrom: this.timeToTimestamp(day.startTime),
							timeTo: this.timeToTimestamp(day.endTime)
						});
					}
				}

			} else if (this.rule.weekValue > 0) {
				for (let i = 0; i < this.data.result.validity.timeIntervals.length; i++) {
					this.data.result.validity.timeIntervals[i].push({
						timeFrom: this.timeToTimestamp(this.rule.wholeWeek.startTime),
						timeTo: this.timeToTimestamp(this.rule.wholeWeek.endTime)
					});
				}
			}
			this.data.result.vehicleIdList = [];
			this.data.result.fleetIdList = [];
			// add vehicles and fleets
			if (this.vehicles.value) {
				this.vehicles.value.forEach(element => {
					this.data.result.vehicleIdList.push(element);
					this.rule.vehicleList.push(element);
				});
			}

			if (this.fleets.value) {
				this.fleets.value.forEach(element => {
					this.data.result.fleetIdList.push(element);
					this.rule.fleetList.push(element);
				});
			}
			this.dialogRef.close();
		}
	}

	onNoClick(): void {
		this.dialogRef.close();
	}

	checkErrors(): boolean {
		let result = true;
		this.errorMessage = undefined;
		if (this.rule.name == undefined || this.rule.name.length === 0 || this.rule.name.length > 100) {
			this.errorMessage = "The rule name is empty or invalid";
			result = false;
		} else if (this.rule.weekValue > 0) {
			if (this.rule.weekValue === 1) {
				if (!this.validateTime(this.rule.wholeWeek.startTime) || !this.validateTime(this.rule.wholeWeek.endTime)) {
					this.errorMessage = "Some of the whole week time inputs is invalid";
					result = false;
				}
			} else {
				let numberOfChecked = 0;
				this.rule.workdays.forEach(day => {
					if (day.checked) {
						numberOfChecked++;
						if (!this.validateTime(day.startTime) || !this.validateTime(day.endTime)) {
							this.errorMessage = "Some of the work days time inputs is invalid";
							result = false;
						}
					}
				});
				this.rule.weekends.forEach(day => {
					if (day.checked) {
						numberOfChecked++;
						if (!this.validateTime(day.startTime) || !this.validateTime(day.endTime)) {
							this.errorMessage = "Some of the weekend time inputs is invalid";
							result = false;
						}
					}
				});
				if (numberOfChecked == 0) {
					this.errorMessage = "No day was selected";
					result = false;
				}
			}
		} else if (!this.validateDate(this.rule.dateFrom) || (!this.rule.endlessRule && (!this.rule.dateTo || !this.validateDate(this.rule.dateTo)))) {
			this.errorMessage = "Invalid dates";
			result = false;
		} else if (!this.rule.endlessRule) {
			let dayCount1 = this.daysFromEpoch(this.rule.dateFrom, true);
			let dayCount2 = this.daysFromEpoch(this.rule.dateTo, false);
			if (dayCount1 > dayCount2) {
				this.errorMessage = "Date from must not be greater than Date to";
				result = false;
			}
		}
		return result;
	}

	validateDate(date): boolean {
		return (date != undefined && date != null);
	}

	ngOnInit() {
	}


	getErrorMessage(input) {
		return input.hasError('required') ? 'You must enter a value' : '';
	}

	validateTime(time): boolean {
		let regex = new RegExp(timeFormat24);
		let result = regex.test(time);
		return result;
	}

	getTimeErrorMessage(error): string {
		let message: string = "Invalid";
		if (error.required) {
			message = "Empty";
		}
		return message;
	}

	onEndlessClick() {
		if (!this.rule.endlessRule) {
			this.rule.dateTo = null;
		}
	}

	checkDate(date): boolean {
		debugger
		let result: boolean = false;
		let dateStr: string;
		if (date != undefined && date != null) {
			dateStr = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
			result = this.dateRegex.test(dateStr);
		}

		return result;
	}


	isNewRuleDialog(): boolean {
		return this.dialogMode == "new_rule";
	}

	isEditDialog(): boolean {
		return this.dialogMode == "edit_rule";
	}

	isDuplicateDialog(): boolean {
		return this.dialogMode == "duplicate_rule";
	}

	daysFromEpoch(date: Date, startingdate: boolean): number {
		let oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
		let diffDays: number = 0;
		if (!startingdate && this.rule.endlessRule) {
			diffDays = 300000;
		} else {
			let dateFrom = new Date(0);
			diffDays = Math.round(Math.abs((date.getTime() - dateFrom.getTime()) / (oneDay)));
		}
		return diffDays;
	}

	timeToTimestamp(time: string): number {
		let result: number = 0;
		if (time) {
			let arr = time.split(":");
			result = ((+arr[0]) * 3600) + ((+arr[1]) * 60);
		}
		return result;
	}


}
