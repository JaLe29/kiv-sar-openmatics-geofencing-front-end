import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator } from '@angular/material';
import { RuleDialogComponent } from './rule-dialog/rule-dialog.component';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { RuleDetailComponent } from './rule-detail/rule-detail.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Rule } from '../../../shared/classes/rule';
import { RulefilterService } from '../../../shared/services/rulefilter.service';
import { Vehicle, Fleet } from '../../../shared/classes/fleet';

const END_POINT: string = '/rules/';
const VEHICLE_ENDPOINT: string = '/vehicles';
const FLEET_ENDPOINT: string = '/fleets';
const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

@Component({
	selector: 'app-rules',
	templateUrl: './rules.component.html',
	styleUrls: ['./rules.component.css']
})
export class RulesComponent implements OnInit {
	displayedColumns = ['name', 'validDate', 'events', 'timeRest', 'desc'];
	ruleDatabase: RuleDatabase = new RuleDatabase();
	dataSource: RuleTableDataSource | null;
	activeRow: null;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	areaId: string;
	allRules: Rule[] = [];
	fleetList: Fleet[] = [];
	vehicleList: Vehicle[] = [];

	result = {
		id: null,
		name: null,
		description: null,
		validity: {
			validationInterval: {
				dateFrom: null,
				dateTo: null,
			},
			timeIntervals: [[], [], [], [], [], [], []]
		},
		areaList: [{
			id: this.areaId
		}],
		vehicleIdList: [],
		fleetIdList: [],
		type: null,
		active: false
	};


	constructor(public dialog: MatDialog, private http: HttpClient, private rulefilter: RulefilterService) {

	}

	ngOnInit() {
		// reading data from backend
		this.rulefilter.currentAreaId.subscribe(id => { this.areaId = id; this.filterRule(); });
		this.getAllRules();
		this.http.get(environment.apiUrl + VEHICLE_ENDPOINT).subscribe(data => {
			for (let i = 0, len = Object.keys(data).length; i < len; i++) {
				this.vehicleList.push(new Vehicle(data[i].id, data[i].name));
			}
		});
		this.http.get(environment.apiUrl + FLEET_ENDPOINT).subscribe(data => {
			for (let i = 0, len = Object.keys(data).length; i < len; i++) {
				this.fleetList.push(new Fleet(data[i].id, data[i].name, data[i].vehicleList));
			}
		});
		this.dataSource = new RuleTableDataSource(this.ruleDatabase, this.paginator);
	}

	getAllRules() {
		this.http.get(environment.apiUrl + END_POINT).subscribe(data => {
			this.allRules = [];
			for (let i = 0, len = Object.keys(data).length; i < len; i++) {
				let rule: Rule = new Rule(data[i]);
				this.allRules.push(rule);
			}
			this.filterRule();
		})
	}

	filterRule() {
		this.result.areaList[0].id = this.areaId;
		let newData: Rule[] = [];
		if (this.areaId) {
			for (let i = 0; i < this.allRules.length; i++) {
				let singleRule: Rule = this.allRules[i];
				if (singleRule.areaId == this.areaId) {
					newData.push(singleRule);
				}
			}
		}

		this.ruleDatabase.setDatabase(newData);
		// also remove existing selection
		this.activeRow = null;
		this.rowClicked(this.activeRow);
	}

	rowClicked(row) {
		if (row == null || this.activeRow === row.id) {
			this.activeRow = null;
			return;
		}
		this.activeRow = row.id;
	}


	/**
	 * For openning new dialog for creating new rule.
	 */
	openNewRuleDialog(): void {
		let ruleCopy: Rule = new Rule();
		var result = Object.assign({}, this.result);
		let dialogRef = this.dialog.open(RuleDialogComponent, {
			width: '40%',
			data: {
				mode: "new_rule", title: "New rule", areaId: this.areaId, data: ruleCopy, result: result, vehicleList: this.vehicleList, fleetList: this.fleetList
			}
		});

		dialogRef.afterClosed().subscribe(returned => {
			// if ok button clicked and data verified
			if (result.active) {
				result.id = null;
				this.http.post(environment.apiUrl + "/rules/", result,
					{ headers: headers }).subscribe(
					(res: any) => {
						let newRule: Rule = new Rule(res);
						this.ruleDatabase.addRuleFirst(newRule);
						this.allRules.unshift(newRule);
					},
					(code) => { }
					);

			}

		});
	}

	duplicateRule(): void {
		// deep copy
		var result = Object.assign({}, this.result);
		let ruleCopy: Rule = Object.assign({}, this.ruleDatabase.getRuleFromId(this.activeRow));
		let dialogRef = this.dialog.open(RuleDialogComponent, {
			width: '40%',
			data: {
				mode: "duplicate_rule", title: "Duplicate rule", areaId: this.areaId, data: ruleCopy, result: result, vehicleList: this.vehicleList, fleetList: this.fleetList

			}
		});

		dialogRef.afterClosed().subscribe(returned => {
			if (result.active) {
				result.id = null;
				this.http.post(environment.apiUrl + "/rules/", result,
					{ headers: headers }).subscribe(
					(res: any) => {
						let newRule: Rule = new Rule(res);
						this.ruleDatabase.addRuleFirst(newRule);
						this.allRules.unshift(newRule);
					},
					(code) => { }
					);

			}

		});
	}

	showRuleDetail() {
		if (this.activeRow != undefined && this.activeRow != null) {
			let rule: Rule = this.ruleDatabase.getRuleFromId(this.activeRow);
			let dialogRef = this.dialog.open(RuleDetailComponent, {
				width: '30%',
				data: {
					rule: rule, vehicleList: this.vehicleList, fleetList: this.fleetList
				}
			});

			dialogRef.afterClosed().subscribe(result => {
				//
			});
		}
	}

	/**
	 * Openning dialog for editing, need to pass the selected rule (deep copy of it)
	 */
	openEditRuleDialog(): void {
		// deep copy
		var result = Object.assign({}, this.result);
		let ruleCopy: Rule = Object.assign({}, this.ruleDatabase.getRuleFromId(this.activeRow));

		let dialogRef = this.dialog.open(RuleDialogComponent, {
			width: '40%',
			data: {
				mode: "edit_rule", title: "Edit rule", areaId: this.areaId, data: ruleCopy, result: result, vehicleList: this.vehicleList, fleetList: this.fleetList
			}
		});

		dialogRef.afterClosed().subscribe(returned => {
			if (result.active) {
				this.http.put(environment.apiUrl + "/rules/", result,
					{ headers: headers }).subscribe(
					(res: any) => {
						for (var i = 0; i < this.allRules.length; i++) {
							if (this.allRules[i].id === ruleCopy.id) {
								this.allRules[i] = new Rule(res);
								break;
							}
						}
						this.filterRule();
					},
					(code) => { }
					);
			}
		});
	}


	deleteRule(): void {
		this.http.delete(environment.apiUrl + END_POINT + this.activeRow).subscribe(() => {
			for (let i = 0; i < this.allRules.length; i++) {
				if (this.allRules[i].id === this.activeRow) {
					this.allRules.splice(i, 1);
					break;
				}
			}
		});
		this.ruleDatabase.removeRule(this.activeRow);
		this.activeRow = null;
	}


	getName(rule): string {
		return rule.name;
	}
	getDesc(rule): string {
		return rule.description;
	}
	getValidDate(rule): string {
		let result: string;
		if (rule.dateFrom == undefined || rule.dateFrom == null) {
			result = "Not found";
		} else {
			result = [rule.dateFrom.getMonth() + 1, rule.dateFrom.getDate(), rule.dateFrom.getFullYear()].join("/");
		}
		result += " - "
		if (rule.endlessRule || rule.dateTo == undefined || rule.dateTo == null) {
			result += "infinity";
		} else {
			result += [rule.dateTo.getMonth() + 1, rule.dateTo.getDate(), rule.dateTo.getFullYear()].join("/");
		}
		return result;
	}
	getEventTypes(rule): string {
		let result: string;
		if (rule.type == 'inside') {
			result = 'Inside';
		} else {
			result = 'Outside';
		}
		return result;
	}
	getTimeRestrictions(rule): string {
		let result: string = "";
		if (rule.weekValue == 0) {
			result = "Always";
		} else if (rule.weekValue == 1) {
			result = "Whole week";
		} else if (rule.weekValue == 2) {
			let first: boolean = true;
			rule.workdays.forEach(element => {
				if (element.checked) {
					if (first) {
						result = element.name;
						first = false;
					} else {
						result += ", " + element.name;
					}
				}
			});
			rule.weekends.forEach(element => {
				if (element.checked) {
					if (first) {
						result += element.name;
						first = false;
					} else {
						result += ", " + element.name;
					}
				}
			});
		} else {
			result = "Not found"; // should not happend
		}
		return result;
	}

}

export class RuleDatabase {
	dataChange: BehaviorSubject<Rule[]> = new BehaviorSubject<Rule[]>([]);
	get data(): Rule[] { return this.dataChange.value; }

	constructor() {

	}

	setDatabase(rules: Rule[]) {
		const copiedData: Rule[] = [];
		for (let i = 0; i < rules.length; i++) {
			let singleRule: Rule = rules[i];
			copiedData.push(singleRule);
		}
		this.dataChange.next(copiedData);
	}

	addRule(newRule: Rule) {
		const copiedData = this.data.slice();
		copiedData.push(newRule);
		this.dataChange.next(copiedData);
	}

	addRuleFirst(newRule: Rule) {
		const copiedData = this.data.slice();
		copiedData.unshift(newRule);
		this.dataChange.next(copiedData);
	}


	removeRule(ruleId) {
		let copiedData = this.data.slice();
		for (let i = 0; i < copiedData.length; i++) {
			if (copiedData[i].id === ruleId) {
				copiedData.splice(i, 1);
				break;
			}
		}
		this.dataChange.next(copiedData);
	}

	getRuleFromId(ruleId): Rule {
		let result: Rule;
		let copiedData = this.data.slice();
		for (let i = 0; i < copiedData.length; i++) {
			if (copiedData[i].id === ruleId) {
				result = copiedData[i];
				break;
			}
		}
		return result;
	}
}


export class RuleTableDataSource extends DataSource<any> {
	constructor(private _ruleDatabase: RuleDatabase, private _paginator: MatPaginator) {
		super();
	}

	/** Connect function called by the table to retrieve one stream containing the data to render. */
	connect(): Observable<Rule[]> {
		const displayDataChanges = [
			this._ruleDatabase.dataChange,
			this._paginator.page,
		];

		return Observable.merge(...displayDataChanges).map(() => {
			const data = this._ruleDatabase.data.slice();

			// Grab the page's slice of data.
			const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
			return data.splice(startIndex, this._paginator.pageSize);
		});
	}

	disconnect() { }
}

