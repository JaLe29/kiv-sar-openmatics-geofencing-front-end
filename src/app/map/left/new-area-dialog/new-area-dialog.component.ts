import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CreateareaService } from '../../../shared/services/createarea.service';

@Component({
	selector: 'app-new-area-dialog',
	templateUrl: './new-area-dialog.component.html',
	styleUrls: ['./new-area-dialog.component.css']
})
export class NewAreaDialogComponent implements OnInit {

	name: string;

	constructor(
		public dialogRef: MatDialogRef<NewAreaDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private createArea: CreateareaService) { }

	ngOnInit() { }

	onCloseClick = () => {
		this.dialogRef.close()
	}

	onCreateClick = () => {
		if (this.name) {
			this.dialogRef.close(this.name);
			this.createArea.activateDrawingTools();

		}
	}
}
