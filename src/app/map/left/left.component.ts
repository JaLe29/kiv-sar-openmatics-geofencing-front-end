import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { MatDialog, MatPaginator } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import { environment } from './../../../environments/environment';
import { NewAreaDialogComponent } from './new-area-dialog/new-area-dialog.component';
import { NewAreaSnackbarComponent } from './new-area-snackbar/new-area-snackbar.component';
import { CreateareaService } from '../../shared/services/createarea.service';
import { RulefilterService } from '../../shared/services/rulefilter.service'
import { DisplayareaService } from '../../shared/services/displayarea.service';

@Component({
	selector: 'app-left',
	templateUrl: './left.component.html',
	styleUrls: ['./left.component.css']
})
export class LeftComponent implements OnInit, OnDestroy {

	displayedColumns = ['created', 'description', 'autor'];
	localDb = new AreaDatabase();
	dataSource: AreaDataSource | null;
	activeRow: null;
	selectedAreaId: string;
	constructor(private http: HttpClient, public dialog: MatDialog, public snackBar: MatSnackBar,
		private createArea: CreateareaService, private displayArea: DisplayareaService, private rulefilter: RulefilterService) {
		this.createArea.refreshAreas.subscribe((refresh: Boolean) => {
			if (refresh === true) {
				this.localDb.removeData();
				this.http.get(environment.apiUrl + '/areas').subscribe(data => {
					this.localDb.addData(data);
				});
				this.createArea.tableRefreshed();
			}
		});
	}

	@ViewChild(MatPaginator) paginator: MatPaginator;

	ngOnInit() {
		this.rulefilter.currentAreaId.subscribe(id => this.selectedAreaId = id);
		this.dataSource = new AreaDataSource(this.localDb, this.paginator);

		this.http.get(environment.apiUrl + '/areas').subscribe(data => {
			this.localDb.addData(data);
		});
	}

	ngOnDestroy() {
		this.createArea.resetArea();
		this.createArea.deactivateDrawingTools();
		if (this.snackBar._openedSnackBarRef != null) {
			this.snackBar._openedSnackBarRef.dismiss();
		}
	}

	rowClicked(row) {
		if (!row || this.activeRow === row.id) {
			this.activeRow = null;
			this.displayArea.changeArea(null);
			this.rulefilter.changeArea(null);
			return;
		}
		this.displayArea.changeArea(row.area);
		this.activeRow = row.id;
		// call the service: rulefilter
		this.rulefilter.changeArea(row.id);
	}

	addArea = () => {

		this.displayArea.changeArea('');
		this.activeRow = null;
		this.dialog
			.open(NewAreaDialogComponent, {})
			.afterClosed()
			.subscribe(result => {
				if (result) {
					this.snackBar.openFromComponent(NewAreaSnackbarComponent, { data: result });
				}
			});
	}

	deleteArea = () => {
		this.localDb.removeItem(this.activeRow);
		this.http.delete(environment.apiUrl + "/areas/" + this.activeRow).subscribe(() => {
			this.activeRow = null;
		});
		this.activeRow = null;
		this.rowClicked(null);

	}
}

export interface UserData {
	id: string;
	created: Date;
	description: string;
	autor: string;
	area: any;
}

export class AreaDatabase {

	dataChange: BehaviorSubject<UserData[]> = new BehaviorSubject<UserData[]>([]);
	get data(): UserData[] { return this.dataChange.value; }

	constructor() { }

	addData(json) {
		let allAreas: any[] = [];
		json.forEach(element => {
			allAreas.push(element);
		});

		allAreas.sort((e1, e2) => (e1.createDateMillis > e2.createDateMillis) ? 1 : (e1.createDateMillis < e2.createDateMillis) ? -1 : 0);
		allAreas.forEach(element => {
			this.addRow(element);
		});
	}
	removeData() {
		let copiedData = [];
		this.dataChange.next(copiedData);
	}

	addRow(row) {
		const copiedData = this.data.slice();
		copiedData.unshift({
			id: row.id,
			created: new Date(row.createDateMillis),
			description: row.name,
			autor: 'Anonymous',
			area: row
		});
		this.dataChange.next(copiedData);
	}

	removeItem(itemId) {
		let copiedData = this.data.slice();
		for (let i = 0; i < copiedData.length; i++) {
			if (copiedData[i].id === itemId) {
				copiedData.splice(i, 1);
				break;
			}
		}
		this.dataChange.next(copiedData);
	}
}

export class AreaDataSource extends DataSource<any> {

	constructor(private _exampleDatabase: AreaDatabase, private _paginator: MatPaginator) {
		super();
	}

	connect(): Observable<UserData[]> {
		const displayDataChanges = [
			this._exampleDatabase.dataChange,
			this._paginator.page,
		];

		return Observable.merge(...displayDataChanges).map(() => {
			const data = this._exampleDatabase.data.slice();

			const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
			return data.splice(startIndex, this._paginator.pageSize);
		});
	}

	disconnect() { }
}
