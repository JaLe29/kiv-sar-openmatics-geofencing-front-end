import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewAreaSnackbarComponent } from './new-area-snackbar.component';

describe('NewAreaSnackbarComponent', () => {
	let component: NewAreaSnackbarComponent;
	let fixture: ComponentFixture<NewAreaSnackbarComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [NewAreaSnackbarComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(NewAreaSnackbarComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
