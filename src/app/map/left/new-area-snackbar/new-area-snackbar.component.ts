import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { MatSnackBarRef, MAT_SNACK_BAR_DATA } from '@angular/material';
import { CreateareaService } from '../../../shared/services/createarea.service';
import { Shape } from '../../../shared/classes/shape';
import { OnDestroy } from '@angular/core';
import { Area } from '../../../shared/classes/area';

@Component({
	selector: 'app-new-area-snackbar',
	templateUrl: './new-area-snackbar.component.html',
	styleUrls: ['./new-area-snackbar.component.css'],
	encapsulation: ViewEncapsulation.None
})
export class NewAreaSnackbarComponent implements OnInit, OnDestroy {

	name: string;
	shape: Shape;

	constructor(
		public snackRef: MatSnackBarRef<NewAreaSnackbarComponent>,
		@Inject(MAT_SNACK_BAR_DATA) public data: any,
		private createArea: CreateareaService) {
		this.name = data;
		createArea.currentShape.subscribe(shape => { this.shape = shape; });
	}
	ngOnDestroy() {
	}
	onCloseClick = () => {
		this.snackRef.dismiss();
		this.createArea.deactivateDrawingTools();
		this.createArea.resetArea();
	}

	onCreateClick = () => {
		if (this.shape.shape == null) {
			//
		} else {
			const area: Area = new Area();
			area.id = 'null';
			area.type = this.shape.type;
			area.name = this.name;
			// create new area
			if (this.shape.type === 'RECTANGLE') {
				const topRight = this.shape.shape.getBounds().getNorthEast();
				const botLeft = this.shape.shape.getBounds().getSouthWest();
				const botLeftFormatted = { 'latitude': botLeft.lat(), 'longitude': botLeft.lng() };
				const topRightFormatted = { 'latitude': topRight.lat(), 'longitude': topRight.lng() };
				area.positionList.push(botLeftFormatted);
				area.positionList.push(topRightFormatted);



			} else if (this.shape.type === 'CIRCLE') {
				const center = this.shape.shape.getCenter();
				const point2 = this.shape.shape.getBounds().getNorthEast();

				area.positionList.push(this.latlng(center.lat(), center.lng()));
				area.positionList.push(this.latlng(point2.lat(), center.lng()));


			} else if (this.shape.type === 'POLYGON') {
				for (let i = 0; i < this.shape.shape.getPath().length; i++) {
					area.positionList.
						push(this.latlng(this.shape.shape.getPath().getAt(i).lat(), this.shape.shape.getPath().getAt(i).lng()));
				}
				area.positionList.
					push(this.latlng(this.shape.shape.getPath().getAt(0).lat(), this.shape.shape.getPath().getAt(0).lng()));
			}
			const response: string = this.createArea.createArea(JSON.stringify(area));
			if (response === '200') {
				this.createArea.refreshTable();
			}
			this.onCloseClick();


		}
	}
	latlng(latitude: any, longitude: any) {
		return { 'latitude': latitude, 'longitude': longitude };
	}
	ngOnInit() { }
}
