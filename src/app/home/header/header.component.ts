import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { FilterService } from '../../shared/services/filter.service';
import { Filter } from '../../shared/classes/filter';
@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
	dateFormat: String = '^(0?[1-9]|1[0-2])([/])(0?[1-9]|1[0-9]|2[0-9]|3[0,1])([/])(19|20|21)[0-9]{2}$';
	filter: Filter;
	today: Date;
	dateFrom: Date;
	dateTo: Date;
	dateRegex: RegExp;
	form: FormControl;
	constructor(private filterService: FilterService) {
	}

	ngOnInit() {
		this.filterService.filterObs.subscribe(filter => { this.filter = filter; });

	}

	log() {
		this.filterService.changeFilter(this.filter);
	}

}
