import { Component, OnInit, } from '@angular/core';
import {} from '@types/googlemaps';
import { AreaService } from '../../shared/services/area.service';
declare let google: any;
@Component({
  selector: 'app-googlemap',
  templateUrl: './googlemap.component.html',
  styleUrls: ['./googlemap.component.css']
})
export class GooglemapComponent implements OnInit {
    map: google.maps.Map;
    message: string;
    changeLog: string[] = [];
    marker: google.maps.Marker;
    currentArea: any;
    drawnAreas: any[] = [];
	currentAgent: any;

    constructor ( private area: AreaService) {
        setTimeout(function() { google.maps.event.trigger(this.map, 'resize'); }, 600);
     }

    ngOnInit() {
        this.message = '';
        this.initMap();
        this.area.currentArea.subscribe(area => {
            this.reset();
            this.currentArea = area;
            if (area) {
                this.drawShape();
            }
        });
        this.reset();
    }
    reset() {
        for (let i = 0; i < this.drawnAreas.length; i++) {
            this.drawnAreas[i].setMap(null);
        }
    }
    initMap() {
        this.map = new google.maps.Map(document.getElementById('map'), {
           center: {lat: 30, lng: 25},
            zoom: 2
        });
    }

    drawShape() {
        for (let i = 0; i < this.currentArea.length; i++) {
            const name = this.currentArea[0].type;
            if (name === 'RECTANGLE') {
                this.drawRectangle(this.currentArea[i].positionList);
            } else if (name === 'POLYGON') {
                this.drawPolygon(this.currentArea[i].positionList);
            } else if (name === 'CIRCLE') {
                this.drawCircle(this.currentArea[i].positionList);
            }
        }

        this.map.setZoom(2);
    }

    drawRectangle(positions: any[]) {
        const topLeftLat: number = positions[0].latitude;
        const topLeftLong: number = positions[0].longitude;
        const botRightLat: number = positions[1].latitude;
        const botRightLong: number = positions[1].longitude;
        const rect = new google.maps.Rectangle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: this.map,
            bounds: {
              north: topLeftLat,
              south: botRightLat,
              east: botRightLong,
              west: topLeftLong
            }
          });
          this.drawnAreas.push(rect);
	}

    drawPolygon(positions: any[]) {
        const paths: google.maps.LatLng[] = [];
        for (let i = 0; i < positions.length; i++) {
            paths.push(new google.maps.LatLng(positions[i].latitude, positions[i].longitude));
        }
        const polygon = new google.maps.Polygon({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: this.map,
            paths: paths
          });
          this.drawnAreas.push(polygon);
	}

    drawCircle(positions: any[]) {
        const paths: google.maps.LatLng[] = [];
        const centerLat: number = positions[0].latitude;
        const centerLong: number = positions[0].longitude;
        const radius: any =
            Math.sqrt(Math.pow((centerLat - positions[1].latitude), 2) + Math.pow((centerLong - positions[1].longitude), 2));
        const center: google.maps.LatLng = new google.maps.LatLng(centerLat, centerLong);

        const dist =
            google.maps.geometry.spherical.computeDistanceBetween
                    (center, new google.maps.LatLng(positions[1].latitude, positions[1].longitude));

        const circle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: this.map,
            center: center,
            radius: dist
          });
          this.drawnAreas.push(circle);
	}

    drawMarker() {
        const myLatLng = {lat: -25.363, lng: 131.044};
        this.marker = new google.maps.Marker({
            position: myLatLng,
            map: this.map,
            title: 'Hello World!'
          });
    }

}
