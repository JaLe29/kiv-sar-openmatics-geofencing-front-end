import { Component, ViewChild, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AreaService } from '../../shared/services/area.service';
import { FilterService } from '../../shared/services/filter.service';
import { Filter } from '../../shared/classes/filter';
import { environment } from './../../../environments/environment';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';

@Component({
	selector: 'app-messages',
	templateUrl: './messages.component.html',
	styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

	filter: Filter;
	message: string;
	displayedColumns = ['date', 'event', 'carid', 'rule'];
	exampleDatabase = new ExampleDatabase();
	dataSource: ExampleDataSource | null;
	activeRow: null;

	constructor(private http: HttpClient, private data: AreaService, private filterService: FilterService) { }
	@ViewChild(MatPaginator) paginator: MatPaginator;

	ngOnInit() {
		this.http.get(environment.apiUrl + '/events').subscribe(evData => {
			this.http.get(environment.apiUrl + '/vehicles').subscribe(vehData => {

				for (var prop in evData) {
					//update vehicle name from second req
					evData[prop].vehName = this.getVehicleNameById(evData[prop].vehicleId, vehData);
				}

				this.exampleDatabase.addData(evData)
				this.dataSource = new ExampleDataSource(this.exampleDatabase, this.paginator);
				this.filterService.filterObs.subscribe(filter => { this.filter = filter; this.filterData(); });
			})
		});
	}

	filterData() {
		this.exampleDatabase.filterDat(this.filter);
	}

	rowClicked(row) {
		if (this.activeRow === row.id) {
			this.activeRow = null;
			this.showAreaOnMap(null);
			return;
		}
		this.activeRow = row.id;
		this.showAreaOnMap(row.areaList);
	}

	showAreaOnMap(list) {
		this.data.changeArea(list);
	}

	getVehicleNameById(id, vehicleLish) {
		for (var prop in vehicleLish) {
			if (vehicleLish[prop].id === id) {
				return vehicleLish[prop].name
			}
		}
		return "Unknown vehicle"
	}
}

export interface UserData {
	id: String;
	vehName: String;
	date: Date;
	event: String;
	carid: String;
	rule: String;
	name: String;
	description: String;
	areaList: any;
}

export class ExampleDatabase {
	private copiedData: UserData[];

	dataChange: BehaviorSubject<UserData[]> = new BehaviorSubject<UserData[]>([]);
	get data(): UserData[] { return this.dataChange.value; }
	public clearFilter() {
		this.dataChange.value.splice(0, this.dataChange.value.length);
		this.dataChange.next(this.copiedData.slice());
	}
	public filterDat(filter: Filter) {
		if (!this.copiedData) {
			return;
		}
		if ((filter.car == null || filter.car === '') && filter.dateFrom == null &&
			filter.dateTo == null && (filter.lastXEntries == null || filter.lastXEntries === 0)
			&& (filter.rule == null || filter.rule === '')) {
			if (this.copiedData.length !== this.dataChange.value.length) {
				this.clearFilter();
			}
			return;
		}
		const allData = this.copiedData.slice();
		let filteredData: UserData[];
		filteredData = [];
		let data: UserData;
		for (let i = 0; i < allData.length; i++) {
			data = allData[i];
			if (filter.dateFrom != null && filter.dateTo != null) {

				if (filter.dateFrom > data.date || filter.dateTo < data.date) {
					continue;
				}
			} else if (filter.dateFrom != null) {
				if (filter.dateFrom > data.date) {
					continue;
				}

			} else if (filter.dateTo != null) {
				if (filter.dateTo < data.date) {
					continue;
				}
			}

			// add car filter
			if (filter.car != null && filter.car !== '') {
				if (filter.car !== data.carid) {
					continue;
				}
			}

			// add rule filter
			if (filter.rule != null && filter.rule !== '') {
				if (filter.rule !== data.rule) {
					continue;
				}
			}

			if (filter.lastXEntries != null && filter.lastXEntries !== 0) {
				const lastDate = new Date();
				lastDate.setDate(lastDate.getDate() - filter.lastXEntries);
				if (lastDate > data.date) {
					continue;
				}
			}
			filteredData.push(allData[i]);

		}

		this.dataChange.value.splice(0, this.dataChange.value.length);
		this.dataChange.next(filteredData);
	}

	constructor() { }

	addData(json) {
		json.forEach(element => {
			this.addRow(element);
		});

		this.copiedData = this.data.slice();
	}

	addRow(row) {
		this.copiedData = this.data.slice();
		this.copiedData.push({
			id: row.id,
			vehName: row.vehName,
			date: new Date(row.eventTime * 1000),
			event: row.rule.name,
			carid: row.vehicleId,
			rule: row.rule.id,
			name: row.name,
			description: row.rule.description,
			areaList: row.rule.areaList
		});
		this.dataChange.next(this.copiedData.slice());
	}
}

export class ExampleDataSource extends DataSource<any> {

	constructor(private _exampleDatabase: ExampleDatabase, private _paginator: MatPaginator) {
		super();
	}

	connect(): Observable<UserData[]> {
		const displayDataChanges = [
			this._exampleDatabase.dataChange,
			this._paginator.page,
		];

		return Observable.merge(...displayDataChanges).map(() => {
			const data = this._exampleDatabase.data.slice();

			const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
			return data.splice(startIndex, this._paginator.pageSize);
		});
	}

	disconnect() { }
}
