import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MapComponent } from './map/map.component';
import { NavComponent } from './shared/nav/nav.component';
import { LeftComponent } from './map/left/left.component';
import { RightComponent } from './map/right/right.component';
import { RulesComponent } from './map/right/rules/rules.component';
import { SubmapComponent } from './map/right/submap/submap.component';
import { MessagesComponent } from './home/messages/messages.component';
import { GooglemapComponent } from './home/googlemap/googlemap.component';
import { HeaderComponent } from './home/header/header.component';
import { RuleDialogComponent } from './map/right/rules/rule-dialog/rule-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import {
	MatAutocompleteModule,
	MatButtonModule,
	MatButtonToggleModule,
	MatCardModule,
	MatCheckboxModule,
	MatChipsModule,
	MatDatepickerModule,
	MatDialogModule,
	MatExpansionModule,
	MatGridListModule,
	MatIconModule,
	MatInputModule,
	MatListModule,
	MatMenuModule,
	MatNativeDateModule,
	MatPaginatorModule,
	MatProgressBarModule,
	MatProgressSpinnerModule,
	MatRadioModule,
	MatRippleModule,
	MatSelectModule,
	MatSidenavModule,
	MatSliderModule,
	MatSlideToggleModule,
	MatSnackBarModule,
	MatSortModule,
	MatTableModule,
	MatTabsModule,
	MatToolbarModule,
	MatTooltipModule,
	MatStepperModule,
	MatSnackBar,
} from '@angular/material';
import { FilterService } from './shared/services/filter.service';
import { AreaService } from './shared/services/area.service';
import { CreateareaService } from './shared/services/createarea.service';
import { RulefilterService } from './shared/services/rulefilter.service'
import { DisplayareaService } from './shared/services/displayarea.service';
import { RuleDetailComponent } from './map/right/rules/rule-detail/rule-detail.component';
import { NewAreaDialogComponent } from './map/left/new-area-dialog/new-area-dialog.component';
import { NewAreaSnackbarComponent } from './map/left/new-area-snackbar/new-area-snackbar.component';
const appRoutes: Routes = [
	{ path: '', component: HomeComponent },
	{ path: 'map', component: MapComponent }
];

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		MapComponent,
		NavComponent,
		LeftComponent,
		RightComponent,
		RulesComponent,
		SubmapComponent,
		MessagesComponent,
		GooglemapComponent,
		RuleDialogComponent,
		HeaderComponent,
		RuleDetailComponent,
		NewAreaDialogComponent,
		NewAreaSnackbarComponent,
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		RouterModule.forRoot(appRoutes),
		MatGridListModule,
		MatButtonModule,
		MatMenuModule,
		MatToolbarModule,
		MatTabsModule,
		MatTableModule,
		MatPaginatorModule,
		FormsModule,
		MatDialogModule,
		MatInputModule,
		MatCheckboxModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatCardModule,
		MatRadioModule,
		MatSelectModule,
		ReactiveFormsModule,
		MatExpansionModule,
		MatIconModule,
		HttpClientModule,
		MatSnackBarModule
	],
	providers: [
		AreaService,
        FilterService,
		CreateareaService,
		RulefilterService,
        DisplayareaService
	],
	bootstrap: [AppComponent
	],
	entryComponents: [
		RuleDialogComponent,
		RuleDetailComponent,
		NewAreaDialogComponent,
		NewAreaSnackbarComponent
	]
})
export class AppModule { }