export class Fleet {
	id: string;
	name: string;
	vehicleList: Vehicle[];

	constructor(id: string, name: string, vehicleList: Vehicle[]) {
		this.id = id;
		this.name = name;
		this.vehicleList = vehicleList;
	}
}

export class Vehicle {
	id: string;
	name: string;

	constructor(id: string, name: string) {
		this.id = id;
		this.name = name;
	}
}