
import { Fleet, Vehicle } from './fleet';
export class Rule {

	id: string;
	name: string; // the rule name
	description: string; // the rule description
	weekValue: number = 0; // the time restriction option, 0-always, 1-whole week, 2-single days
	endlessRule: boolean = false; // true for endless valid date
	dateFrom: Date; // starting date
	dateTo: Date; // ending date
	vehicleList: string[] = [];
	fleetList: string[] = [];
	areaId: string = null;


	wholeWeek = { startTime: "", endTime: "" };
	weekends = [
		{ name: "Sat", checked: false, startTime: "", endTime: "" },
		{ name: "Sun", checked: false, startTime: "", endTime: "" }
	];

	workdays = [
		{ name: "Mon", checked: false, startTime: "", endTime: "" },
		{ name: "Tue", checked: false, startTime: "", endTime: "" },
		{ name: "Wen", checked: false, startTime: "", endTime: "" },
		{ name: "Thu", checked: false, startTime: "", endTime: "" },
		{ name: "Fri", checked: false, startTime: "", endTime: "" }
	];

	type: string = "inside";


	constructor(data?) {
		if (data) { // reading data
			this.name = data.name;
			this.description = data.description;
			this.id = data.id;

			this.vehicleList = (data.vehicleList) ? data.vehicleList : [];
			this.fleetList = (data.fleetList) ? data.fleetList : [];

			this.setDate(data.validity.validationInterval);
			this.setTime(data.validity.timeIntervals);
			this.areaId = (data.areaList && data.areaList.length > 0) ? data.areaList[0].id : null;
			this.setVehicles(data.vehicleIdList);
			this.setFleets(data.fleetIdList);
			if (data.type === 'OUTSIDE') {
				this.type = 'outside';
			}
		} else {
			this.dateFrom = new Date();
			this.dateTo = new Date();

		}
	}

	setVehicles(vehicles) {
		if (vehicles) {
			for (let i = 0; i < vehicles.length; i++) {
				let v = vehicles[i];
				this.vehicleList.push(v);
			}

		}
	}

	setFleets(fleets) {
		if (fleets) {
			for (let i = 0; i < fleets.length; i++) {
				let f = fleets[i];
				this.fleetList.push(f);
			}

		}
	}

	setDate(validationInterval) {
		this.dateFrom = this.daysToDate(validationInterval.dateFrom);
		if (validationInterval.dateTo < 300000) // very very far away
		{
			this.dateTo = this.daysToDate(validationInterval.dateTo);
			this.endlessRule = false;
		} else {
			this.dateTo = null;
			this.endlessRule = true;
		}
	}

	setName(name: string) { this.name = name; }

	setDescription(desc: string) { this.description = desc; }


	setTime(timeIntervals) {
		this.weekValue = 0; // default is always
		if (timeIntervals) {
			let different: boolean = false;
			let validCount: number = 0;

			for (let i = 0; i < timeIntervals.length; i++) {
				let element = timeIntervals[i];
				if (element && element.length == 1) {
					let startTimeTimeStamp: number = element[0].timeFrom;
					let endTimeTimeStamp: number = element[0].timeTo;

					if (startTimeTimeStamp < endTimeTimeStamp) {
						// valid time range
						validCount++;
						let startTime: string = this.timestampToTime(startTimeTimeStamp);
						let endTime: string = this.timestampToTime(endTimeTimeStamp);
						if (i >= this.workdays.length) {
							// weekend
							let index: number = i - this.workdays.length;
							this.weekends[index].checked = true;
							this.weekends[index].startTime = startTime;
							this.weekends[index].endTime = endTime;
						} else {
							// normal day
							this.workdays[i].checked = true;
							this.workdays[i].startTime = startTime;
							this.workdays[i].endTime = endTime;
						}
					}
				}
			}

			if (validCount > 0) {
				// user defined
				this.weekValue = 2;

				if (validCount == timeIntervals.length && this.isWholeWeek()) {
					// if all 7 days are the same
					this.wholeWeek.startTime = this.workdays[0].startTime;
					this.wholeWeek.endTime = this.workdays[0].endTime;
					this.weekValue = 1;
				}
			}


		}
	}

	isWholeWeek(): boolean {
		let result: boolean = true;
		let start: string = this.workdays[0].startTime;
		let end: string = this.workdays[0].endTime;
		// test equality
		for (let i = 1; i < this.workdays.length; i++) {
			if (this.workdays[i].startTime != start || this.workdays[i].endTime != end) {
				result = false;
				break;
			}
		}
		if (!result) {
			for (let i = 0; i < this.weekends.length; i++) {
				if (this.weekends[i].startTime != start || this.weekends[i].endTime != end) {
					result = false;
					break;
				}
			}
		}
		return result;
	}

	daysToDate(days: number): Date {
		let oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
		var date = new Date(days * oneDay);
		return date;
	}

	daysFromEpoch(date: Date, startingDate: boolean): number {
		let oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
		let diffDays: number;
		if (!startingDate && !date) {
			diffDays = 300000;
		} else {
			let dateFrom = new Date(0);
			diffDays = Math.round(Math.abs((date.getTime() - dateFrom.getTime()) / (oneDay)));
		}

		return diffDays;
	}

	timestampToTime(timestamp: number): string {
		let result: string;
		let hours: number = Math.floor(timestamp / (3600));
		let minutes: number = Math.floor((timestamp - (hours * 3600)) / 60);
		result = (hours < 10) ? "0" + hours + ":" : hours + ":";
		result += (minutes < 10) ? "0" + minutes : minutes + "";
		return result;
	}

	timeToTimestamp(time: string): number {
		let result: number = 0;
		if (time) {
			let arr = time.split(":");
			result = ((+arr[0]) * 3600) + ((+arr[1]) * 60);
		}
		return result;
	}

	setId(id: string) { this.id = id; }

	setEventTypes(eventType: string) {
		if (eventType || eventType == 'INSIDE') {
			this.type = "inside";
		} else {
			this.type = "outside";
		}
	}




}