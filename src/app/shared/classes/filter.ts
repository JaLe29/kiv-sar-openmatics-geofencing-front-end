export class Filter {
	dateFrom: Date;
	dateTo: Date;
	lastXEntries: number;
	car: String;
	area: String;
	rule: String;
	name: string;
	constructor() {
		this.dateFrom = null;
		this.dateTo = null;
		this.lastXEntries = null;
		this.car = null;
		this.area = null;
		this.rule = null;
	}
}
