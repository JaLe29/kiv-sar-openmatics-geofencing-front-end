import { TestBed, inject } from '@angular/core/testing';

import { DisplayareaService } from './displayarea.service';

describe('DisplayareaService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [DisplayareaService]
		});
	});

	it('should be created', inject([DisplayareaService], (service: DisplayareaService) => {
		expect(service).toBeTruthy();
	}));
});
