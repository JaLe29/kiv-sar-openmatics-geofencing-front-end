import { TestBed, inject } from '@angular/core/testing';

import { CreateareaService } from './createarea.service';

describe('CreateareaService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [CreateareaService]
		});
	});

	it('should be created', inject([CreateareaService], (service: CreateareaService) => {
		expect(service).toBeTruthy();
	}));
});
