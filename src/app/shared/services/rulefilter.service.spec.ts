import { TestBed, inject } from '@angular/core/testing';

import { RulefilterService } from './rulefilter.service';

describe('RulefilterService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [RulefilterService]
		});
	});

	it('should be created', inject([RulefilterService], (service: RulefilterService) => {
		expect(service).toBeTruthy();
	}));
});
