import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Shape } from '../classes/shape';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions } from '@angular/http';
import { environment } from '../../../environments/environment';
import { Headers } from '@angular/http';
import { Area } from '../classes/area';
@Injectable()
export class CreateareaService {
	endpoint = '/areas';
	private activate = new BehaviorSubject<Boolean>(false);
	activateDrawing = this.activate.asObservable();
	private shape = new BehaviorSubject<Shape>(new Shape());
	currentShape = this.shape.asObservable();
	private refresh = new BehaviorSubject<Boolean>(false);
	refreshAreas = this.refresh.asObservable();
	constructor(private http: HttpClient) { }
	public activateDrawingTools() {
		this.activate.next(true);
	}
	public deactivateDrawingTools() {
		this.activate.next(false);
	}
	public resetArea() {
		this.shape.next(new Shape());
	}
	public refreshTable() {
		this.refresh.next(true);
	}
	public tableRefreshed() {
		this.refresh.next(false);
	}
	public createArea(area: any): string {
        /*const headers = new Headers();
        headers.append('Content-Type', 'application/json');*/
		const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
		// const options = new RequestOptions({ headers: headers });
		let response: string;
		const a = this.http.post(environment.apiUrl + this.endpoint, area,
			{ headers: headers }).subscribe(
			(res: any) => {
				// this.postResponse = res;
				response = '200';
				this.refreshTable();
			},
			(x) => {
				/* this function is executed when there's an ERROR */
				response = x;
			},
			() => {
				/* this function is executed when the observable ends (completes) its stream */
			}
			);
		return response;
	}

}
