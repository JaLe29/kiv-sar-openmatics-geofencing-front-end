import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Filter } from '../classes/filter';
@Injectable()
export class FilterService {
	private filter = new BehaviorSubject<Filter>(new Filter());
	filterObs = this.filter.asObservable();

	constructor() { }
	changeFilter(filter: Filter) {
		this.filter.next(filter);
	}

}
