import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class RulefilterService {
	private areaId = new BehaviorSubject<string>(null);
	currentAreaId = this.areaId.asObservable();

	constructor() { }

	changeArea(areaId: string) {
		this.areaId.next(areaId);
	}
}

