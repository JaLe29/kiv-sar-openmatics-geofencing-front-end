import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
@Injectable()
export class AreaService {
	private area = new BehaviorSubject<any>('');
	currentArea = this.area.asObservable();
	constructor() { }
	changeArea(newArea: any) {
		this.area.next(newArea);
	}
}
