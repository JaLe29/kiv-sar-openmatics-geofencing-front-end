import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

	headerImgPath: string;
	footerImgPath: string;

	ngOnInit() {
		this.footerImgPath = '/assets/images/om_logo_footer.svg'
		this.headerImgPath = 'assets/images/openmatics_logo.png'
	}
}

